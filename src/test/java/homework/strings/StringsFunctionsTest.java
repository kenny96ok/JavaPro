package homework.strings;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static homework.strings.StringsFunctions.*;
import static org.junit.jupiter.api.Assertions.*;

class StringsFunctionsTest {
    @ParameterizedTest
    @MethodSource("occuranceStrings")
    void shouldReturnCountOccurance(String source, char key, int count) {
        assertEquals(count, findSymbolOccurance(source, key));
    }

    static Stream<Arguments> occuranceStrings() {
        return Stream.of(
                Arguments.of("sos", 's', 2),
                Arguments.of("Occurance", 'c', 3),
                Arguments.of("return", 'r', 2)
        );
    }

    @ParameterizedTest
    @MethodSource("wordPositionStrings")
    void shouldReturnWordPosition(String source, String target, int position) {
        assertEquals(position, findWordPosition(source, target));
    }

    static Stream<Arguments> wordPositionStrings() {
        return Stream.of(
                Arguments.of("Apollo", "pollo", 1),
                Arguments.of("Apple", "plant", -1),
                Arguments.of("return", "turn", 2)
        );
    }

    @Test
    void shouldReturnStringReverse() {
        assertEquals("olleH", stringReverse("Hello"));
    }

    @Test
    void shouldCheckStringForPalindrome() {
        assertTrue(isPalindrome("ERE"));
        assertFalse(isPalindrome("Allo"));
    }
}