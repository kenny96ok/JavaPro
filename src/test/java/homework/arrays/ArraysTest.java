package homework.arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ArraysTest {
    @ParameterizedTest
    @NullAndEmptySource
    void shouldThrowRuntimeException_whenNullOrEmptyValues(int[] emptyValues) {
        assertThrowsExactly(RuntimeException.class, () -> Arrays.getSubArrayAfterFour(emptyValues));
    }

    @Test
    void shouldThrowRuntimeException_whenFourNotIncluded() {
        assertThrowsExactly(RuntimeException.class, () -> Arrays.getSubArrayAfterFour(new int[]{1, 2, 3}));
    }

    @ParameterizedTest
    @MethodSource("arrayInputOutputValuesIntArrays")
    void shouldReturnArrayAfterFour(int[] input, int[] expected) {
        assertArrayEquals(Arrays.getSubArrayAfterFour(input), expected);
    }

    static Stream<Arguments> arrayInputOutputValuesIntArrays() {
        return Stream.of(
                Arguments.of(new int[]{4, 1, 2, 3}, new int[]{1, 2, 3}),
                Arguments.of(new int[]{1, 2, 3, 4}, new int[0]),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 1, 7}, new int[]{5, 1, 7}),
                Arguments.of(new int[]{1, 2, 3, 4, 1, 2, 3, 4, 5, 6, 7, 8, 9}, new int[]{5, 6, 7, 8, 9})
        );
    }

    @ParameterizedTest
    @NullAndEmptySource
    void shouldThrowRuntimeException_whenNullOrEmptyValuesSecondMethod(int[] emptyValues) {
        assertThrowsExactly(RuntimeException.class, () -> Arrays.checkArrayMustIncludeOnlyOneAndFour(emptyValues));
    }

    @ParameterizedTest
    @MethodSource("arrayInputOutputValuesIntArrayAndBoolean")
    void shouldCheckArrayContainOneAndFour(int[] input, boolean expected) {
        assertEquals(expected, Arrays.checkArrayMustIncludeOnlyOneAndFour(input));
    }

    static Stream<Arguments> arrayInputOutputValuesIntArrayAndBoolean() {
        return Stream.of(
                Arguments.of(new int[]{1, 4}, true),
                Arguments.of(new int[]{1, 1, 1, 4, 4, 1, 4, 4}, true),
                Arguments.of(new int[]{1, 1, 1, 1, 1, 1}, false),
                Arguments.of(new int[]{4, 4, 4, 4}, false),
                Arguments.of(new int[]{1, 4, 4, 1, 1, 4, 3}, false),
                Arguments.of(new int[]{0, 2, 3, 5, 6, 7, 8, 9}, false)
        );
    }
}