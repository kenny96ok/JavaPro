package homework.set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static homework.set.SetFunctions.*;

class SetFunctionsTest {
    private List<String> words = new ArrayList<>();

    SetFunctionsTest() {
        words.addAll(Arrays.stream(new String[]{"Привет", "Мир", "Привет", "!", "!"}).toList());
    }

    @Test
    void shouldReturnUniqueListStrings() {
        String[] expected = {"Привет", "Мир", "!"};
        assertArrayEquals(Arrays.stream(expected)
                        .sorted(Comparator.comparingInt(String::length))
                        .toArray(),
                convertToUnique(words).toArray());
    }

    @ParameterizedTest
    @MethodSource("arrayInputOutputValuesIntArrayASC")
    void shouldReturnUniqueSortedIntegersASC(int[] source, int[] expected) {
        List<Integer> list = new ArrayList<>();
        for (int i : source) {
            list.add(i);
        }
        int i = 0;
        for (Integer integer : getSortedUniqueIntegersASC(list)) {
            assertEquals(expected[i++], integer);
        }
    }

    static Stream<Arguments> arrayInputOutputValuesIntArrayASC() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 4, 5, 6, 4, 5, 6}, new int[]{1, 2, 3, 4, 5, 6}),
                Arguments.of(new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 4}),
                Arguments.of(new int[]{1, 2, 3, 4, 1, 2, 3, 4, 5, 6, 7, 8, 9}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9})
        );
    }

    @ParameterizedTest
    @MethodSource("arrayInputOutputValuesIntArraysDESC")
    void shouldReturnUniqueSortedIntegersDESC(int[] source, int[] expected) {
        List<Integer> list = new ArrayList<>();
        for (int i : source) {
            list.add(i);
        }
        int i = 0;
        for (Integer integer : getSortedUniqueIntegersDESC(list)) {
            assertEquals(expected[i++], integer);
        }
    }

    static Stream<Arguments> arrayInputOutputValuesIntArraysDESC() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 4, 5, 6, 4, 5, 6}, new int[]{6, 5, 4, 3, 2, 1}),
                Arguments.of(new int[]{1, 2, 3, 4}, new int[]{4, 3, 2, 1}),
                Arguments.of(new int[]{1, 2, 3, 4, 1, 2, 3, 4, 5, 6, 7, 8, 9}, new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1})
        );
    }

    @Test
    void shouldReturnSentenceDirectionUniqueListStrings() {
        String[] expected = {"Привет", "Мир", "!"};
        assertArrayEquals(Arrays.stream(expected).toArray(),
                buildSentenceDirection(words).toArray());
    }
}