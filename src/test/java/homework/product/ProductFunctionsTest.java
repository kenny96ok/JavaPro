package homework.product;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ProductFunctionsTest {
    List<Product> products = new ArrayList<>();

    public ProductFunctionsTest() {
        products.add(new Product(Type.BOOK, 200, true, LocalDate.parse("2022-05-30")));
        products.add(new Product(Type.BOOK, 255, false, LocalDate.parse("2022-05-31")));
        products.add(new Product(Type.TOY, 300, true, LocalDate.parse("2022-06-01")));
    }

    @Test
    void shouldReturnListOfBooksByPrice() {
        List<Product> expected = new ArrayList<>();
        expected.add(products.get(1));
        assertArrayEquals(expected.toArray(),
                ProductFunctions.getProductsByTypeAndExpensivePrice(products, Type.BOOK, 250).toArray());
    }

    @Test
    void shouldReturnListOfDiscountedBooks() {
        assertEquals(180,
                ProductFunctions.getProductsByTypeAndHaveDiscount(products, Type.BOOK, 10)
                        .get(0).getPrice());
    }

    @Test
    void shouldReturnCheappestBook() {
        assertEquals(200, ProductFunctions.getСheapestProductByType(products, Type.BOOK).getPrice());
        products.remove(0);
        products.remove(0);
        Exception exception = assertThrows(RuntimeException.class,
                () -> ProductFunctions.getСheapestProductByType(products, Type.BOOK));
        assertTrue(exception.getMessage().contains("Продукт [категория: BOOK] не найден"));
    }

    @Test
    void shouldReturnThreeLastAddedProduct() {
        products.add(new Product(Type.TOY, 300, true, LocalDate.parse("2022-06-02")));
        List<Product> lastThree = ProductFunctions.getLastAddedProducts(products, 3);
        for (int i = 0; i < 3; i++) {
            assertTrue(lastThree.contains(products.get(i + 1)));
        }
    }

    @Test
    void shouldReturnTotalCoastOfProductsByParams() {
        products.get(0).setPrice(50);
        products.get(1).setPrice(75);
        products.get(2).setPrice(50);
        assertEquals(125, ProductFunctions.getTotalCostOfProductsByTypeAndYearAndPriceNotExceed(products,
                Type.BOOK, 2022, 75));
    }

    @Test
    void shouldReturnGroupedProducts() {
        Map<Type, List<Product>> groupedProducts = ProductFunctions.getGroupedProducts(products);
        assertEquals(2, groupedProducts.keySet().size());
        assertEquals(2, groupedProducts.get(Type.BOOK).size());
        assertEquals(1, groupedProducts.get(Type.TOY).size());
    }
}