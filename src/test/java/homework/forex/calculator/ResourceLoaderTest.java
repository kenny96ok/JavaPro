package homework.forex.calculator;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class ResourceLoaderTest {

    @Test
    void shouldRetrieveResource_thatMustContainsFxRatesData_butNotEmpty() {
        assertThat(new ResourceLoader().load()).isNotEmpty();
    }

    @Test
    void shouldRetrieveResource_thatMustContainsFxRatesData_butEmpty() {
        assertThat(new ResourceLoader().load()).isEmpty();
    }

    @Test
    void shouldThrowRuntimeException_whenResourceWithFxRatesNotFound() {
        assertThat(catchRuntimeException(() -> new ResourceLoader().load())).isNotNull();
    }
}