package homework.human;

public class Human {
    private String firstName;
    private String surName;
    private String patronymic;

    public Human(String FirstName, String SurName) {
        this.firstName = FirstName;
        this.surName = SurName;
    }

    public Human(String FirstName, String SurName, String Patronymic) {
        this.firstName = FirstName;
        this.surName = SurName;
        this.patronymic = Patronymic;
    }

    public String getFullName() {
        return surName + " " + firstName + (patronymic != null ? " " + patronymic : "");
    }

    public String getShortName() {
        return surName + " " + firstName.substring(0, 1) + (patronymic != null ? ". " + patronymic.substring(0, 1) : "") + ".";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Human human = (Human) o;

        if (!firstName.equals(human.firstName))
            return false;
        if (!surName.equals(human.surName))
            return false;
        return patronymic != null ? patronymic.equals(human.patronymic)
                : human.patronymic == null;
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + surName.hashCode();
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Human{" +
                "FirstName='" + firstName + '\'' +
                ", SurName='" + surName + '\'' +
                ", Patronymic='" + patronymic + '\'' +
                '}';
    }
}
