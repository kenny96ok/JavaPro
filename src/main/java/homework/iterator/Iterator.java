package homework.iterator;

import java.util.Arrays;

public class Iterator {
    private int size;
    private int count;
    private int[] arr;

    public Iterator(int[][] arr) {
        this.count=0;

        for (int[] ints : arr) {
            this.size += ints.length;
        }

        this.arr=new int[size];
        for (int i = 0, k = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                this.arr[k++]=arr[i][j];
            }
        }

        this.arr = Arrays.stream(this.arr).sorted().toArray();
    }

    public boolean hasNext() {
        return count < size;
    }

    public int next() {
        return this.arr[count++];
    }
}
