package homework.file.logger;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class FileLoggerConfiguration {
    private File logFile;
    private final LoggingLevel level;
    private final int sizeOfFileInBytes;
    private final String format;

    public FileLoggerConfiguration(String fileName, LoggingLevel level, int sizeOfFileInBytes, String format) {
        createNewFile(fileName);
        this.level = level;
        this.sizeOfFileInBytes = sizeOfFileInBytes;
        this.format = format;
    }

    public LoggingLevel getLevel() {
        return level;
    }

    public File getLogFile() {
        return logFile;
    }

    public int getSizeOfFileInBytes() {
        return sizeOfFileInBytes;
    }

    public void createNewFile(String fileName) {
        this.logFile = new File(fileName);
        try {
            if (!logFile.exists())
                logFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFinalMessage(String methodName, String message) {
        int splitSize = format.split("\\[CURRENT_TIME]").length;
        String result = format.split("\\[CURRENT_TIME]")[0]
                + getTime() + ((splitSize == 2) ? format.split("\\[CURRENT_TIME]")[1] : "");

        splitSize = result.split("\\[LEVEL]").length;
        result = result.split("\\[LEVEL]")[0]
                + methodName + ((splitSize == 2) ? result.split("\\[LEVEL]")[1] : "");

        splitSize = result.split("\\[MESSAGE]").length;
        result = result.split("\\[MESSAGE]")[0]
                + message + ((splitSize == 2) ? result.split("\\[MESSAGE]")[1] : "");
        return result;
    }

    private String getTime() {
        Calendar calendar = Calendar.getInstance();
        return (calendar.get(Calendar.HOUR_OF_DAY) + ":"
                + calendar.get(Calendar.MINUTE) + ":"
                + calendar.get(Calendar.SECOND));
    }
}
