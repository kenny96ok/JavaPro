package homework.file.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import static homework.file.logger.LoggingLevel.*;

public class FileLogger {
    private final FileLoggerConfiguration configuration;

    /**
     * @param level  A {@code LoggingLevel} enum
     * @param format A format string must contain three required string-keys:<br/>
     *               [LEVEL] - {@code LoggingLevel} name,<br/>
     *               [CURRENT_TIME] - current time in log string,<br/>
     *               [MESSAGE] - message in log string.<br/>
     *               Example: "[CURRENT_TIME] [LEVEL] Message: [MESSAGE]"
     */
    public FileLogger(LoggingLevel level, String format) {
        String fileName = "Log_" + getDate() + "_" + getTime() + ".txt";
        this.configuration = new FileLoggerConfiguration(fileName, level, 8192, format);
    }

    /**
     * @param level  A {@code LoggingLevel} enum
     * @param sizeFile  A integer size of log file in bytes
     * @param format A format string must contain three required string-keys:<br/>
     *               [LEVEL] - {@code LoggingLevel} name,<br/>
     *               [CURRENT_TIME] - current time in log string,<br/>
     *               [MESSAGE] - message in log string.<br/>
     *               Example: "[CURRENT_TIME] [LEVEL] Message: [MESSAGE]"
     */
    public FileLogger(LoggingLevel level, int sizeFile, String format) {
        String fileName = "Log_" + getDate() + "_" + getTime() + ".txt";
        this.configuration = new FileLoggerConfiguration(fileName, level, sizeFile, format);
    }

    public void warning(String message) {
        if (configuration.getLevel() == INFO || configuration.getLevel() == DEBUG) {
            System.out.println("You don't have access to WARNING");
            return;
        }
        checkSizeOfFile();
        writeInFile(WARNING.name(), message);
    }

    public void debug(String message) {
        if (configuration.getLevel() == INFO) {
            System.out.println("You don't have access to DEBUG");
            return;
        }
        checkSizeOfFile();
        writeInFile(DEBUG.name(), message);
    }

    public void info(String message) {
        checkSizeOfFile();
        writeInFile(INFO.name(), message);
    }

    private String getTime() {
        Calendar calendar = Calendar.getInstance();
        return (calendar.get(Calendar.HOUR_OF_DAY) + "."
                + calendar.get(Calendar.MINUTE) + "."
                + calendar.get(Calendar.SECOND));
    }

    private String getDate() {
        Calendar calendar = Calendar.getInstance();
        return (calendar.get(Calendar.DATE) + "."
                + calendar.get(Calendar.MONTH) + "."
                + calendar.get(Calendar.YEAR));
    }

    private void checkSizeOfFile() {
        if(configuration.getLogFile().length()>=configuration.getSizeOfFileInBytes()) {
            String fileName = "Log_" + getDate() + "_" + getTime() + ".txt";
            configuration.createNewFile(fileName);
        }
    }

    private void writeInFile(String methodName, String message) {
        try (FileWriter fileWriter = new FileWriter(configuration.getLogFile(), true)) {
            fileWriter.write(configuration.getFinalMessage(methodName, message) + "\n");
            fileWriter.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
