package homework.file.navigator;

import java.nio.file.Path;

public class FileData {
    private String name;
    private long sizeInBytes;
    private String path;

    public FileData(String name, long sizeInBytes, String path) {
        this.name = name;
        this.sizeInBytes = sizeInBytes;
        if (path.contains(name))
            this.path = Path.of(path.split(name)[0]).toString();
        else
            this.path = Path.of(path).toString();
    }

    public String getName() {
        return name;
    }

    public long getSizeInBytes() {
        return sizeInBytes;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return "FileData{" +
                "name='" + name + '\'' +
                ", sizeInBytes=" + sizeInBytes +
                ", path='" + path + '\'' +
                '}';
    }
}
