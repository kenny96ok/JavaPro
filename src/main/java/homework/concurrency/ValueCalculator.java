package homework.concurrency;

public class ValueCalculator {
    private float[] array;
    private int size;
    private int halfSize;

    public ValueCalculator(int size) {
        this.size = size;
        this.halfSize = size / 2;
        this.array = new float[size];
    }

    public void doCalc() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            array[i] = i + 1;
        }
        float[] firstHalf = new float[halfSize], secondHalf = new float[size - halfSize];
        System.arraycopy(array, 0, firstHalf, 0, halfSize);
        System.arraycopy(array, halfSize, secondHalf, 0, size - halfSize);

        Thread first = new Thread(() -> {
            for (int i = 0; i < halfSize; i++) {
                firstHalf[i] = (float) (firstHalf[i] * Math.sin(0.2f + i / 5f) * Math.cos(0.2f + i / 5f) * Math.cos(0.4f + i / 2f));
            }
        }), second = new Thread(() -> {
            for (int i = 0; i < size - halfSize; i++) {
                secondHalf[i] = (float) (secondHalf[i] * Math.sin(0.2f + i / 5f) * Math.cos(0.2f + i / 5f) * Math.cos(0.4f + i / 2f));
            }
        });
        first.start();
        second.start();
        try {
            first.join();
            second.join();
        } catch (Exception ex) {
            System.out.println("Thread.join() exception: " + ex.getMessage());
        }

        System.arraycopy(firstHalf, 0, array, 0, halfSize);
        System.arraycopy(secondHalf, 0, array, halfSize, size-halfSize);

        System.out.println(System.currentTimeMillis()-start + " ms");
    }
}
