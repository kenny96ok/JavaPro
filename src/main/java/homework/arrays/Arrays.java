package homework.arrays;

public class Arrays {
    static int[] getSubArrayAfterFour(int[] source) {
        if (source == null || source.length == 0)
            throw new RuntimeException("Array must not be null or empty.");
        int i = source.length - 1;
        for (; i >= 0; i--)
            if (source[i] == 4)
                break;
        if (i == -1)
            throw new RuntimeException("Array must contain at least one 4");
        int[] result = new int[source.length - (++i)];
        System.arraycopy(source, i, result, 0, source.length - i);
        return result;
    }

    static boolean checkArrayMustIncludeOnlyOneAndFour(int[] source) {
        if (source == null || source.length == 0)
            throw new RuntimeException("Array must not be null or empty.");
        int countOnes = 0, countFours = 0;
        for (int i = 0; i < source.length; i++) {
            if (source[i] == 1) countOnes++;
            if (source[i] == 4) countFours++;
        }
        return (countOnes + countFours) == source.length && countOnes != 0 && countFours != 0;
    }
}
