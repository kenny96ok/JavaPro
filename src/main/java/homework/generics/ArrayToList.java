package homework.generics;

import java.util.Arrays;
import java.util.List;

public class ArrayToList<T> {
    public List<T> arrayToList(T[] array) {
        return Arrays.stream(array).toList();
    }
}
