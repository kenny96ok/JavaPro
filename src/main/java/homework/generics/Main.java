package homework.generics;

public class Main {
    public static void main(String[] args) {
        String[] strings = {"One", "Two", "Three"};
        new ArrayToList<>().arrayToList(strings).forEach(System.out::println);

        Integer[] ints = {1, 2, 3, 4, 5};
        new ArrayToList<>().arrayToList(ints).forEach(System.out::println);

        Float[] floats = {1f, 2f, 3.14f, 4f, 5f};
        new ArrayToList<>().arrayToList(floats).forEach(System.out::println);
    }
}
