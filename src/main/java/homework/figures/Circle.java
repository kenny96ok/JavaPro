package homework.figures;

public class Circle implements Figure{
    private double radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public double square() {
        return Math.PI * Math.pow(radius, 2);
    }
}
