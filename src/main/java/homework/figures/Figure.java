package homework.figures;

public interface Figure {
    public double square();
}
