package homework.forex.calculator;

public interface Loader<T>{
    T load();
}
