package homework.forex.calculator;

import java.time.LocalDateTime;
import java.util.Currency;

public record FxRate(LocalDateTime dateTime, Currency foreignCurrency,
                     String unit, String nationalCurrencyValue) {
}
