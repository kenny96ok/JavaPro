package homework.forex.calculator;

@FunctionalInterface
public interface Parser<T, R> {
    R parse(T resource);
}
