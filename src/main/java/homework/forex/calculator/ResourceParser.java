package homework.forex.calculator;

import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.time.LocalDateTime;

public class ResourceParser implements Parser<List<InputStream>, List<FxRate>>{

    @Override
    public List<FxRate> parse(List<InputStream> listResource) {
        List<FxRate> result = new ArrayList<>();
        for (InputStream resource : listResource) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
                reader.readLine();
                result.addAll(reader.lines().map(line -> line.split("\\|"))
                        .map(parts -> new FxRate(
                                        LocalDateTime.parse(parts[1].substring(1, parts[1].length() - 4)
                                                + "T" + parts[2].substring(1, parts[2].length() - 2), DateTimeFormatter.ISO_DATE_TIME),
                                        Currency.getInstance(parts[4].substring(1, 4)),
                                        parts[5].substring(1, 2),
                                        parts[7].substring(1,parts[7].length()-3))
                        ).toList());
            } catch (IOException ex) {
                throw new RuntimeException("An error occurred while reading data from files!", ex);
            }
        }
        return result;
    }
}
