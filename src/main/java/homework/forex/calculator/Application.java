package homework.forex.calculator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Currency;

public class Application {
    public static void main(String[] args) {
        FxRate foundRate = null;
        if(args.length == 1) {
            if(args[0].equals("--help")) {
                System.out.println("Usage: name.jar [LETTER_CODE] [[DATE_FROM] [DATE_TO]]");
                System.out.println("This jar returns the arithmetic mean value of the currency, \n" +
                        "you can also specify the boundaries of the data sample by date.");
                System.out.println("DATE_FROM and DATE_TO without an offset, such as '2011-12-03'.\n" +
                        "Date boundaries are not included in the selection.");
                return;
            }
            foundRate = new FxRateParser(Currency.getInstance(args[0])).parse(
                    new ResourceParser().parse(
                            new ResourceLoader().load()));
        }
        if(args.length == 3) {
            foundRate = new FxRateParser(Currency.getInstance(args[0]),
                    LocalDate.parse(args[1], DateTimeFormatter.ISO_LOCAL_DATE),
                    LocalDate.parse(args[2], DateTimeFormatter.ISO_LOCAL_DATE))
                    .parseInTimePeriod( new ResourceParser().parse(new ResourceLoader().load()));

        }
        if(foundRate == null) throw new RuntimeException("Incorrect command syntax!");
        System.out.println("Средняя стоимость UAH [Ukrainian Hryvnia] к "
                + foundRate.foreignCurrency().getCurrencyCode() + " ["
                + foundRate.foreignCurrency().getDisplayName() + "] -> " + foundRate.nationalCurrencyValue());
    }
}
