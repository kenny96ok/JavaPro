package homework.abstraction;

public class Treadmill extends Obstacle{
    public Treadmill(double length) {
        super(length);
    }

    @Override
    public <T extends Participant> boolean overcome(T participant) {
        if(participant.getObstacleLength()>getSize()) {
            System.out.println(participant.getClassName() + " " +
                    participant.getName() + " пробежал беговую дорожку.");
            return true;
        }
        else {
            System.out.println(participant.getClassName() + " " +
                    participant.getName() + " не пробежал беговую дорожку.");
            return false;
        }
    }

}
