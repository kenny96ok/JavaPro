package homework.abstraction;

public class Human extends Participant {
    public Human(String name, int obstacleHeight, int obstacleLength) {
        super("Человек", name);
        setObstacleHeight(obstacleHeight);
        setObstacleLength(obstacleLength);
    }

    @Override
    public void run() {
        System.out.println("Человек " + getName() + " побежал!");
    }

    @Override
    public void jump() {
        System.out.println("Человек " + getName() + " прыгнул!");
    }
}
