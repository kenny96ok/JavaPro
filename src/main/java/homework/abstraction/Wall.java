package homework.abstraction;

public class Wall extends Obstacle {
    public Wall(double height) {
        super(height);
    }

    @Override
    public <T extends Participant> boolean overcome(T participant) {
        if(participant.getObstacleHeight()>getSize()) {
            System.out.println(participant.getClassName() + " " +
                    participant.getName() +" перепрыгнул через стену.");
            return true;
        }
        else {
            System.out.println(participant.getClassName() + " " +
                    participant.getName() + " не перепрыгнул через стену.");
            return false;
        }
    }

}
