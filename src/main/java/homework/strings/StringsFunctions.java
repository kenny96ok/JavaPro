package homework.strings;

public class StringsFunctions {
    public static int findSymbolOccurance(String source, char key) {
        int count = 0;
        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == key)
                count++;
        }
        return count;
    }

    public static int findWordPosition(String source, String target) {
        int result = -1;
        for (int i = 0, j = 0; i < source.length(); i++) {
            if (j + 1 == target.length())
                break;
            if (source.charAt(i) == target.charAt(j)) {
                if (j == 0) result = i;
                j++;
            } else {
                j = 0;
                result = -1;
            }
        }
        return result;
    }

    public static String stringReverse(String source) {
        char[] result = new char[source.length()];
        for (int i = 0; i < source.length(); i++) {
            result[i] = source.charAt(source.length() - i - 1);
        }
        return String.valueOf(result);
    }

    public static boolean isPalindrome(String source) {
        for (int i = 0; i < source.length() / 2; i++) {
            if (source.charAt(i) != source.charAt(source.length() - i - 1))
                return false;
        }
        return true;
    }
}
