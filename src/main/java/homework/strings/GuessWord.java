package homework.strings;

import java.util.Scanner;

public class GuessWord {
    private String[] words;
    private int secretWord;
    private Scanner scanner;

    public GuessWord(String[] words) {
        this.words = words;
        this.secretWord = new java.util.Random().nextInt(words.length);
        this.scanner = new Scanner(System.in);
    }

    public void run() {
        System.out.println("Игра Угадай слово!");
        while (true) {
            char[] result = "###############".toCharArray();
            System.out.print("Ваше слово: ");
            String variant = scanner.nextLine();
            for (int i = 0; i < Math.min(words[secretWord].length(), variant.length()); i++) {
                if (words[secretWord].charAt(i) == variant.charAt(i))
                    result[i] = words[secretWord].charAt(i);
            }
            if (words[secretWord].compareTo(variant) == 0) {
                System.out.println("Вы отгадали!");
                break;
            }
            System.out.println(result);
        }
        System.out.println("Игра окончена.");
    }
}
