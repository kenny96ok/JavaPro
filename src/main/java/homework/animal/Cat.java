package homework.animal;

public class Cat extends Animal implements Runnable {
    private static int catsCount;

    public Cat(String name) {
        super(name);
        catsCount++;
    }

    public static int getCatsCount() {
        return catsCount;
    }

    @Override
    public void run(int meters) {
        System.out.println((meters > 200) ? (getName() + " пробежал 200 метров и устал!") :
                (getName() + " пробежал " + meters + " м."));
    }
}
