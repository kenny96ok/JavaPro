package homework.animal;

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("Мурзик");
        cat.eat();
        cat.run(200);
        cat.run(350);
        cat.run(150);
        Cat cat2 = new Cat("Борис");
        System.out.println("Кол-во котов: " + Cat.getCatsCount());

        Dog dog = new Dog("Бобик");
        dog.eat();
        dog.run(200);
        dog.run(650);
        dog.swim(10);
        dog.swim(15);
        Dog dog2 = new Dog("Рекс");
        System.out.println("Кол-во собак: " + Cat.getCatsCount());

        System.out.println("Общее количество животных: " + Animal.getAnimalCount());
    }
}
