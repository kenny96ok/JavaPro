package homework.animal;

public class Dog extends Animal implements Runnable, Swimmable {
    private static int dogCount;

    public Dog(String name) {
        super(name);
        dogCount++;
    }

    public static int getDogCount() {
        return dogCount;
    }

    @Override
    public void run(int meters) {
        System.out.println((meters > 500) ? (getName() + " пробежал 500 метров и устал!") :
                (getName() + " пробежал " + meters + " м."));
    }

    @Override
    public void swim(int meters) {
        System.out.println((meters > 10) ? (getName() + " проплыл 10 метров и чуть не утонул!") :
                (getName() + " проплыл " + meters + " м."));
    }
}
