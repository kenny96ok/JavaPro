package homework.lists.coffee.order;

public record Order(int orderNumber, String name) {
}
