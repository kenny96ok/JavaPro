package homework.lists.phonebook;

import java.util.ArrayList;
import java.util.List;

public class PhoneBook {
    private List<Record> records = new ArrayList<>();

    public PhoneBook() {
    }

    public void add(String name, String number) {
        records.add(new Record(name, number));
    }

    public Record find(String name) {
        for (Record record : records) {
            if (record.name().equals(name)) {
                return record;
            }
        }
        return null;
    }

    public List<Record> findAll(String name) {
        List<Record> results = new ArrayList<>();
        for (Record record : records) {
            if (record.name().equals(name)) {
                results.add(record);
            }
        }
        return (results.size()>0)?results:null;
    }
}
