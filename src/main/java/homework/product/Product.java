package homework.product;

import java.time.LocalDate;

public class Product {
    private Type type;
    private float price;
    private boolean possibleDiscount;
    private LocalDate createdAt;

    public Product(Type type, float price, boolean possibleDiscount, LocalDate createdAt) {
        this.type = type;
        this.price = price;
        this.possibleDiscount = possibleDiscount;
        this.createdAt = createdAt;
    }

    public Type getType() {
        return type;
    }

    public float getPrice() {
        return price;
    }

    public boolean isPossibleDiscount() {
        return possibleDiscount;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "Product{" +
                "type=" + type +
                ", price=" + price +
                ", possibleDiscount=" + possibleDiscount +
                ", createdAt=" + createdAt +
                '}';
    }
}