package homework.product;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductFunctions {
    public static List<Product> getProductsByTypeAndExpensivePrice(List<Product> products, Type type, float price) {
        return products.stream()
                .filter(product -> product.getType().equals(type) && product.getPrice() > price)
                .toList();
    }

    public static List<Product> getProductsByTypeAndHaveDiscount(List<Product> products, Type type,
                                                                 int discountPercent) {
        return products.stream()
                .filter(product -> product.getType().equals(type) && product.isPossibleDiscount())
                .map(product -> {
                            product.setPrice(product.getPrice() - product.getPrice() * (discountPercent / 100f));
                            return product;
                        }
                )
                .toList();
    }

    public static Product getСheapestProductByType(List<Product> products, Type type) {
        return products.stream()
                .filter(product -> product.getType().equals(type))
                .min((o1, o2) -> Float.compare(o1.getPrice(), o2.getPrice()))
                .orElseThrow(() -> new RuntimeException("Продукт [категория: " + type.name() + "] не найден"));
    }

    public static List<Product> getLastAddedProducts(List<Product> products, int countProducts) {
        return products.stream()
                .sorted(Comparator.comparing(Product::getCreatedAt).reversed())
                .limit(3)
                .toList();
    }

    public static float getTotalCostOfProductsByTypeAndYearAndPriceNotExceed(List<Product> products, Type type,
                                                                             int year, float price) {
        float sum = 0;
        List<Product> sortedProducts = products.stream()
                .filter(product -> product.getType().equals(type) &&
                        product.getCreatedAt().getYear() == year &&
                        product.getPrice() <= price).toList();
        for (Product product : sortedProducts) {
            sum += product.getPrice();
        }
        return sum;
    }

    public static Map<Type, List<Product>> getGroupedProducts(List<Product> products) {
        Map<Type, List<Product>> groupedProducts = new HashMap<>();
        for (Product product : products) {
            groupedProducts.put(product.getType(), null);
        }
        for (Type type : groupedProducts.keySet()) {
            groupedProducts.put(type, products.stream().filter(product -> product.getType().equals(type)).toList());
        }
        return groupedProducts;
    }
}
