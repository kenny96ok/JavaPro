package homework.exceptions;

public class Main {
    public static void main(String[] args) {
        String[][] numbers = {
                {"1", "1", "1", "1"},
                {"1", "1", "1", "1"},
                {"1", "1", "1", "1"},
                {"1", "1", "1", "1"}
        };
        try {
            System.out.println("Сумма всех элементов: " + ArrayValueCalculator.doCalc(numbers));
        } catch (RuntimeException ex) {
            ex.printStackTrace(System.out);
        }
    }
}
