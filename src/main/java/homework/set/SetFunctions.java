package homework.set;

import java.util.*;

public class SetFunctions {
    public static Set<String> convertToUnique(List<String> source) {
        return new HashSet<>(source);
    }

    public static Set<Integer> getSortedUniqueIntegersASC(List<Integer> source) {
        return new HashSet<>(source);
    }

    public static SortedSet<Integer> getSortedUniqueIntegersDESC(List<Integer> source) {
        TreeSet<Integer> set = new TreeSet<>(source);
        return set.descendingSet();
    }

    public static LinkedHashSet<String> buildSentenceDirection(List<String> source) {
        return new LinkedHashSet<>(source);
    }
}
