package homework.collections.custom.linked.list;

import java.util.Iterator;

public interface CustomList<E> {

    int size();

    void add(E element);

    E get(int index);

    boolean remove(int index);

    Iterator<E> iterator();
}
