package homework.collections.custom.linked.list;

import java.util.Objects;

public class DoubleLinkedList<E> implements CustomList<E> {
    private int size = 0;
    private DoubleLinkedList.Node<E> first;
    private DoubleLinkedList.Node<E> last;

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(E element) {
        if (first == null && last == null) {
            first = last = new Node<>(element);
            size++;
            return;
        }
        last.next = new Node<>(element);
        last.next.prev = last;
        last = last.next;
        size++;
    }

    @Override
    public E get(int index) {
        Objects.checkIndex(index, size);

        if (index == 0) return getFirst().value;
        if (index == size - 1) return getLast().value;

        Node<E> current = first;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        return current.value;
    }

    @Override
    public boolean remove(int index) {
        Objects.checkIndex(index, size);

        if (index == 0) return removeFirst();
        if (index == size - 1) return removeLast();

        Node<E> current = first;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }

        current.next.prev = current.prev;
        current.prev.next = current.next;
        size++;
        return true;
    }

    public boolean removeLast() {
        if (first == null && last == null)
            return false;
        if (first == last) {
            first = last = null;
        } else {
            last = last.prev;
        }
        size--;
        return true;
    }

    public boolean removeFirst() {
        if (first == null && last == null)
            return false;
        if (first == last) {
            first = last = null;
        } else {
            first = first.next;
        }
        size--;
        return true;
    }

    @Override
    public DoubleIterator<E> iterator() {
        return new DoubleIterator<>(this);
    }

    public Node<E> getFirst() {
        return first;
    }

    public Node<E> getLast() {
        return last;
    }

    public void set(int index, E element) {
        Objects.checkIndex(index, size);

        if (index == 0) {
            first.value = element;
            return;
        }
        if (index == size - 1) {
            last.value = element;
            return;
        }

        Node<E> current = first;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        current.value = element;
    }

    protected static class Node<E> {
        private E value;
        private DoubleLinkedList.Node<E> next;
        private DoubleLinkedList.Node<E> prev;

        public Node(E value) {
            this.value = value;
        }

        public E getValue() {
            return value;
        }
    }
}
