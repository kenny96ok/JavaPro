package homework.reflection;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class TestRunner {
    public static void start(Class<?> argClass) throws Exception {
        Object objArgClass = argClass.getConstructor().newInstance();
        List<Method> beforeSuite = Arrays.stream(argClass.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(BeforeSuite.class))
                .toList();
        if(beforeSuite.size() > 1) throw new RuntimeException("BeforeSuite should be the only one!");
        if(beforeSuite.size() == 1) beforeSuite.get(0).invoke(objArgClass);

        List<Method> afterSuite = Arrays.stream(argClass.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(AfterSuite.class))
                .toList();
        if(afterSuite.size() > 1) throw new RuntimeException("AfterSuite should be the only one!");
        if(afterSuite.size() == 1) afterSuite.get(0).invoke(objArgClass);

        List<Method> testMethods = Arrays.stream(argClass.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(Test.class))
                .sorted(Comparator.comparingInt(method -> method.getAnnotation(Test.class).order()))
                .toList();
        for (Method method : testMethods) {
            method.invoke(objArgClass);
        }
    }
}
