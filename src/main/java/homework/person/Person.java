package homework.person;

public class Person {
    private String firstName;
    private String surName;
    private String patronymic;

    public Person(String surName, String firstName, String patronymic) {
        this.firstName = firstName;
        this.surName = surName;
        this.patronymic = patronymic;
    }

    public Person(String fullName) {
        this.firstName = fullName.split(" ")[1];
        this.surName = fullName.split(" ")[0];
        this.patronymic = fullName.split(" ")[2];
    }

    public String getFullName() {
        return surName + " " + firstName + " " + patronymic;
    }
}
