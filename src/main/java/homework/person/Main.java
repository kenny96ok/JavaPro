package homework.person;


public class Main {
    public static void main(String[] args) {
        //1
        /*Person man = new Person("Присажнюк", "Олег", "Васильевич");
        Person woman = new Person("Стрижаченко Валентина Алексеевна");
        System.out.println(man.getFullName());
        System.out.println(woman.getFullName());*/
        //2
        /*int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10 - 5);
        }
        System.out.println("Исходный массив:");
        showArray(array);
        array = sortedSquare(array);
        System.out.println("Отсортированный массив квадратов значений:");
        showArray(array);*/
        //3
        permutations("abc");
    }

    public static void showArray(int[] array) {
        for (int elem : array) {
            System.out.println(elem);
        }
    }

    public static int[] sortedSquare(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] * array[i];
        }
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        return array;
    }

    public static void permutations(String str) {
        permutation("", str);
    }

    private static void permutation(String firstPart, String str) {
        if (str.length() != 0) {
            for (int i = 0; i < str.length(); i++)
                permutation(firstPart + str.charAt(i), str.substring(0, i) + str.substring(i + 1));
        } else
            System.out.println(firstPart);
    }
}
