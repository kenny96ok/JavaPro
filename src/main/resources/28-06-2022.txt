| Date          | Time      | Digital code  | Letter code   | Unit  | Currency name     | "UAH"     |
| 2022-06-28    | 00:00:00  | 036           | AUD           | 1     | Australian Dollar | 20.2561   |
| 2022-06-28    | 00:00:00  | 944           | AZN           | 1     | Azerbaijan Manat	| 17.2483   |
| 2022-06-28    | 00:00:00  | 933           | BYN           | 1     | Belarusian Ruble	| 10.6335   |